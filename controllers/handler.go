package controllers

import (
    "net/http"
    "encoding/json"
    "fmt"
    "log"
    "io"
    "io/ioutil"
    "strconv"

    "github.com/gorilla/mux"
    
    "tantan/models"
)

func writeJsonResponse(w http.ResponseWriter, doc interface{}, status int) (err error) {
    w.Header().Set("Content-Type", "application/json; charset=utf-8")
    w.WriteHeader(status)
    encoder := json.NewEncoder(w)
    if err = encoder.Encode(doc); err != nil {
        http.Error(w, "Internal Server Error", http.StatusInternalServerError)
    }
    return
}


func Home(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintln(w, "Welcome to visit the tantan api service!")
}


func GetAllUsers(w http.ResponseWriter, r *http.Request) {
    log.Println("[Info] user listing query from", r.RemoteAddr)
    users, _ := models.GetAllUsers()
    if len(users) > 0 {
        json.NewEncoder(w).Encode(users)
        return
    }
    json.NewEncoder(w).Encode(make([]models.User, 0))
}

func AddUser(w http.ResponseWriter, r *http.Request) {
    log.Println("[Info] user adding query from", r.RemoteAddr)

    var user models.User

    body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
    if err != nil {
        writeJsonResponse(w, "[err400] fail to read request", 400)
        log.Printf("[Error] %v", err)
        return
    }
    

    if err := json.Unmarshal(body, &user); err != nil || user.Name == "" {
        writeJsonResponse(w, "[err422] input data is bad or incomplete", 422)
        log.Printf("[Error] %v", err)
        return
    }

    user.Type = "user"
    err = models.AddUser(&user)
    if err != nil {
        writeJsonResponse(w, "[err500] internal server error", 500)
        log.Printf("[Database Error] %v", err)
        return
    }

    writeJsonResponse(w, user, http.StatusCreated)
}

func GetAllRelationshipsByUserId(w http.ResponseWriter, r *http.Request) {
    log.Println("[Info] relationships query from", r.RemoteAddr)

    vars := mux.Vars(r)
    uid, err := strconv.ParseInt(vars["uid"], 10, 64)
    if err != nil {
        writeJsonResponse(w, "[err422] bad input data", 422)
        log.Printf("[Error] fail to read user id: %v", err)
        return
    }

    relationships, err := models.GetAllRelationshipsByUserId(uid)

    if err != nil {
        http.Error(w, "Internal Server Error", http.StatusInternalServerError)
    }

    writeJsonResponse(w, relationships, http.StatusOK)
}

func RelationshipHandler(w http.ResponseWriter, r *http.Request) {
    log.Println("[Info] relationships update query from", r.RemoteAddr)

    vars := mux.Vars(r)

    uid, err := strconv.ParseInt(vars["uid"], 10, 64)
    if err != nil {
        writeJsonResponse(w, "[err422] bad uid input data", 422)
        log.Printf("[Error] fail to read user id: %v", err)
        return
    }

    tuid, err := strconv.ParseInt(vars["target_uid"], 10, 64)
    if err != nil {
        writeJsonResponse(w, "[err422] bad target_uid input data", 422)
        log.Printf("[Error] fail to read target user id: %v", err)
        return
    }

    body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
    if err != nil {
        writeJsonResponse(w, "[err400] fail to read request", 400)
        log.Printf("[Error] %v", err)
        return
    }

    var relationship models.Relationship
    if err := json.Unmarshal(body, &relationship); err != nil {
        writeJsonResponse(w, "[err422] input data is bad or incomplete", 422)
        log.Printf("[Error] invalid input data for updating relationship: %v", err)
        return
    }

    if relationship.State != "liked" && relationship.State != "disliked" {
        writeJsonResponse(w, "[err422] input state is invalid", 422)
        return
    }

    relationship.Uid = uid
    relationship.TargetUid = tuid
    rp, _ := models.GetRelationshipByUserId(uid, tuid)
    targetRp, _ := models.GetRelationshipByUserId(tuid, uid)

    err = nil // reset err
    if relationship.State == "liked" {
        if targetRp.Id != 0 && targetRp.State == "liked" {
            if rp.Id != 0 {
                relationship.State = "matched"
                err = models.UpdateToMatched(rp.Id, targetRp.Id)
            } else {
                relationship.State = "matched"
                err = models.InsertAndUpdateToMatched(&relationship, targetRp.Id)
            }
        } else { // the other user's state is not 'liked'
            if rp.Id != 0 {
                err = models.UpdateRelationshipState(rp.Id, relationship.State)
            } else {
                err = models.AddRelationship(&relationship)
            }
        }
    } else { // disliked
        if targetRp.Id != 0 && targetRp.State == "matched" {
            if rp.Id != 0 {
                err = models.UpdateToUnmatched(rp.Id, targetRp.Id)
            } else {
                err = models.InsertAndUpdateToMatched(&relationship, targetRp.Id)
            }
        } else {
            if rp.Id != 0 {
                err = models.UpdateRelationshipState(rp.Id, relationship.State)
            } else {
                err = models.AddRelationship(&relationship)
            }
        }
    }

    if err != nil {
        writeJsonResponse(w, "[err500] fail to update this relationship", 500)
        log.Printf("[Error] db fail to update: %v", err)
        return
    }
    
    writeJsonResponse(w, relationship, http.StatusOK)
}