package models

import (
    "gopkg.in/pg.v3"
)

var db *pg.DB

func InitDB(dbUser, dbPasswd, dbName string) {
    db = pg.Connect(&pg.Options{
        User:     dbUser,
        Password: dbPasswd,
        Database: dbName,
    })
}