# Tantan Back-End Developer Test

## Requirements

* Go programming language (tested on v1.5.3)
* Go dependent packages: 
```
go get github.com/gorilla/mux
go get gopkg.in/pg.v3
```
* PostgreSQL

## Deployment

* Download source files from this repo to you $GOPATH/src folder.
```
cd $GOPATH/src
mkdir tantan
git clone https://github.com/kissghosts/tantan-httpserver tantan
```

* Build the tantan service:
```
go install tantan
```

* Edit your conf.json file based on your environment, and copy it to $GOPATH/bin/


* Prepare database environment by running [create-tables.sql](https://github.com/kissghosts/tantan-httpserver/blob/master/sql/create-tables.sql) in the sql folder. This sql script helps to create 2 tables for required task.

* Now the restful server is ready to run :)
```
cd $GOBIN/
sudo ./tantan
```
