SET client_encoding = 'UTF8';

CREATE TABLE users (
    id          serial                                   NOT NULL,
    name        varchar(200)                             NOT NULL,
    type        varchar(32)    DEFAULT 'user'::varchar   NOT NULL,
    CONSTRAINT users_pk PRIMARY KEY(id)
);

-- state: 0 disliked 1 liked 2 matched
CREATE TABLE relationships (
    id          serial                          NOT NULL,
    uid         int                             NOT NULL,
    target_uid  int                             NOT NULL,
    state       varchar(32)                     NOT NULL,
    CONSTRAINT relationships_pk PRIMARY KEY(id),
    CONSTRAINT uid_fk FOREIGN KEY (uid) REFERENCES users(id),
    CONSTRAINT target_uid_fk FOREIGN KEY (target_uid) REFERENCES users(id)
);