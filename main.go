package main

import (
    "log"
    "net/http"
    "encoding/json"
    "os"

    "github.com/gorilla/mux"
    
    "tantan/controllers"
    "tantan/models"
)

type config struct {
    Port        string  `json:"port"`
    DbUser      string  `json:"db_user"`
    DbPasswd    string  `json:"db_passwd"`
    DbName      string  `json:"db_name"`
}

func main() {
    file, err := os.Open("conf.json")
    if err != nil {
        log.Printf("[Error] fail to open conf.json file")
    }
    decoder := json.NewDecoder(file)
    var conf config
    err = decoder.Decode(&conf)
    if err != nil {
        log.Printf("[Warning] fail to read conf.json, use default parameters instead: %v", err)
        conf.Port = "80"
        conf.DbUser = "postgres"
        conf.DbPasswd = "postgres"
        conf.DbName = "postgres"
    }

    models.InitDB(conf.DbUser, conf.DbPasswd, conf.DbName)
    
    router := mux.NewRouter()
    router.HandleFunc("/", controllers.Home)
    router.HandleFunc("/users", controllers.GetAllUsers).Methods("GET")
    router.HandleFunc("/users", controllers.AddUser).Methods("POST")
    router.HandleFunc("/users/{uid}/relationships", 
        controllers.GetAllRelationshipsByUserId).Methods("GET")
    router.HandleFunc("/users/{uid}/relationships/{target_uid}", 
        controllers.RelationshipHandler).Methods("PUT")
    port := ":" + conf.Port
    log.Fatal(http.ListenAndServe(port, router))
}