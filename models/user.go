package models

type User struct {
    Id   int64  `json:"id"`
    Name string `json:"name"`
    Type string `json:"type"`
}

func AddUser(user *User) (err error) {
    _, err = db.QueryOne(user, `INSERT INTO users (name, type) 
        VALUES (?name, ?type) RETURNING id`, user)
    return
}

func GetAllUsers() (users []User, err error) {
    _, err = db.Query(&users, `SELECT * FROM users`)
    return
}