package models

import "gopkg.in/pg.v3"

type Relationship struct {
    Id          int64   `json:"id"`
    Uid         int64   `json:"uid"`
    TargetUid   int64   `json:"target_uid"`
    State       string  `json:"state"`
}

func AddRelationship(reltionship *Relationship) (err error) {
    // fmt.Println(reltionship)
    _, err = db.QueryOne(reltionship, `INSERT INTO relationships 
        (uid, target_uid, state) VALUES (?uid, ?target_uid, ?state) 
        RETURNING id`, reltionship)
    return
}

func GetRelationshipByUserId(uid, tuid int64) (reltionship Relationship, err error) {
    _, err = db.QueryOne(&reltionship, `SELECT * FROM relationships 
        WHERE uid = ? and target_uid = ?`, uid, tuid)
    return
}

func GetAllRelationshipsByUserId(uid int64) (reltionships []Relationship, err error) {
    _, err = db.Query(&reltionships, `SELECT * FROM relationships 
        WHERE uid = ?`, uid)
    return
}

func UpdateRelationshipState(id int64, state string) error {
    _, err := db.ExecOne(`UPDATE relationships SET state = ? WHERE id = ?`, 
        state, id)
    return err
}

//Atomically update the relationship to "matched"
func UpdateToMatched(id, target_id int64) (err error) {

    sqlUpdate := `UPDATE relationships SET state = ? WHERE id = ?`
    err = db.RunInTransaction(func(tx *pg.Tx) error {
            _, err = db.ExecOne(sqlUpdate, "matched", target_id)
            _, err = db.ExecOne(sqlUpdate, "matched", id)
            return err
        })

    return
}

//Atomically update the relationship to "unmatched"
func UpdateToUnmatched(id, target_id int64) (err error) {

    sqlUpdate := `UPDATE relationships SET state = ? WHERE id = ?`
    err = db.RunInTransaction(func(tx *pg.Tx) error {
            _, err = db.ExecOne(sqlUpdate, "liked", target_id)
            _, err = db.ExecOne(sqlUpdate, "disliked", id)
            return err
        })

    return
}

//First insert new entry, then update corresponding relationship to "matched" atomically
func InsertAndUpdateToMatched(relationship *Relationship, target_id int64) (err error) {
    sqlInsert := `INSERT INTO relationships (uid, target_uid, state) 
                VALUES (?uid, ?target_uid, ?state)`
    sqlUpdate := `UPDATE relationships SET state = ? WHERE id = ?`
    err = db.RunInTransaction(func(tx *pg.Tx) error {
            _, err = db.ExecOne(sqlInsert, relationship)
            _, err = db.ExecOne(sqlUpdate, "matched", target_id)
            return err
        })

    return
}